#include "mem.h"
#include "mem_internals.h"

size_t test_passed = 0;

void test(bool test) {
  if (!test) {
    printf("Test %zu failed\n", test_passed);

  } else {
    test_passed++;
    printf("Test %zu passed\n", test_passed);
  }
}

static struct block_header *block_get_header(void *contents)
{
  return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

bool mem_alloc()
{
  heap_init(0);
  void *ptr = _malloc(10);
  if (ptr == NULL) {
    printf("ptr is NULL\n");
    return false;
  }
  heap_term();
  return true;
}

bool free_one_block()
{
  heap_init(0);
  void *ptr1 = _malloc(10);
  struct block_header *block = block_get_header(ptr1);

  if (block->is_free) {
    printf("block is free\n");
    return false;
  }

  _free(ptr1);
  if (!block->is_free) {
    printf("block is not free\n");
    return false;
  }

  void *ptr2 = _malloc(12);
  if (ptr1 != ptr2) {
    printf("ptr1 != ptr2\n");
    return false;
  }

  heap_term();
  return true;
}

bool free_two_blocks()
{
  heap_init(0);
  void *ptr1 = _malloc(10);
  void *ptr2 = _malloc(10);
  struct block_header *block1 = block_get_header(ptr1);
  struct block_header *block2 = block_get_header(ptr2);

  if (block1->is_free || block2->is_free) {
    printf("block_1 or block_2 is free\n");
    return false;
  }

  _free(ptr1);
  if (!block1->is_free || block2->is_free) {
    printf("block_1 is not free or block_2 is free\n");
    return false;
  }

  _free(ptr2);
  if (!block1->is_free || !block2->is_free) {
    printf("block_1 or block_2 is not free\n");
    return false;
  }

  heap_term();
  return true;
}

bool expand_region()
{
  heap_init(10);
  void *ptr = _malloc(12);
  if (HEAP_START + offsetof(struct block_header, contents) != ptr) {
    printf("heap + offsetof(struct block_header, contents) != ptr\n");
    return false;
  }
  heap_term();
  return true;
}

bool old_not_expanded()
{
  void *heap = heap_init(0);
  struct block_header *block = heap;
  void* alloc_data = mmap(heap+REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
  if (alloc_data == NULL) {
    printf("alloc_data is NULL\n");
    return false;
  }
  _malloc(REGION_MIN_SIZE + 2);
  if (!block->is_free) {
    printf("block is not free\n");
    return false;
  }
  heap_term();
  return true;
}

int main()
{
  test(mem_alloc());
  test(free_one_block());
  test(free_two_blocks());
  test(expand_region());
  test(old_not_expanded());
  if (test_passed == 5) {
    printf("All tests passed\n");
  }
  return 0;
}